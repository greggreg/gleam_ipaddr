# ipaddr 🏡

IP Address parsing and printing in Gleam.

Parsing of both v4 and v6 and printing of v4 from rfc3986:

- https://tools.ietf.org/html/rfc3986

Printing of V6 from rfc5952:

- https://tools.ietf.org/html/rfc5952

Documentation: https://hexdocs.pm/ipaddr/<br>
Hex: https://hex.pm/packages/ipaddr

## Usage Gleam

Add `ipaddr` to your deps in `rebar.config`

```erlang
{deps, [
    ipaddr
]}.
```

Then to use:

````gleam
import ipaddr

// supports standard v4
assert Ok(my_v4_ip) = ipaddr.from_string("192.168.0.1") // Ok(ipaddr.IpV4(192, 168, 0, 1))

ipaddr.to_string(my_v4_ip) // "192.168.0.1"

// supports standard v6
assert Ok(my_v6_ip) = ipaddr.from_string("2001:db8::2:1") // Ok(ipaddr.IpV6(8193,3512,0,0,0,0,2,1))

ipaddr.to_string(my_v6_ip) // "2001:db8::2:1"

// supports parsing v6 with trailing v4
assert Ok(my_v6_ip) = ipaddr.from_string("2001:db8::192.168.0.1") // Ok(ipaddr.IpV6(8193,3512,0,0,0,0,49320,1))

// to string always outputs in standard v6
ipaddr.to_string(my_v6_ip) // "2001:db8::c0a8:1"
```


## Usage Elixir

Add `ipaddr` to your deps in `mix.exs`

```elixir
def deps do
  [{:ipaddr, "~> 0.1"}]
end
````

Then to use:

```elixir
// supports standard v4
{:ok, my_ip_v4} = :ipaddr.from_string("192.168.0.1") // Ok(ipaddr.IpV4(192, 168, 0, 1))

:ipaddr.to_string(my_v4_ip) // "192.168.0.1"

// supports standard v6
{:ok, my_v6_ip} = :ipaddr.from_string("2001:db8::2:1") // Ok(ipaddr.IpV6(8193,3512,0,0,0,0,2,1))

:ipaddr.to_string(my_v6_ip) // "2001:db8::2:1"

// supports parsing v6 with trailing v4
{:ok, my_v6_ip} = :ipaddr.from_string("2001:db8::192.168.0.1") // Ok(ipaddr.IpV6(8193,3512,0,0,0,0,49320,1))

// to string always outputs in standard v6
:ipaddr.to_string(my_v6_ip) // "2001:db8::c0a8:1"
```

## Contributions

Welcome!
