//// IP Address parsing and printing per specification.
////
//// Parsing of both v4 and v6 and printing of v4 from rfc3986:
////  - https://tools.ietf.org/html/rfc3986
////
//// Printing of V6 from rfc5952 
////  - https://tools.ietf.org/html/rfc5952

import gleam/bit_string
import gleam/int
import gleam/io
import gleam/list
import gleam/result
import gleam/string

pub type IpAddress {
  IpV4(Int, Int, Int, Int)
  IpV6(Int, Int, Int, Int, Int, Int, Int, Int)
}

/// Parse an Ip Address from the front of a String.
///
/// ## Example Supported Addresses:
///   - Standard v4 : d.d.d.d where d is in decimal
///   - Standard v6 : x:x:x:x:x:x:x:x where x is in hex
///                 | x::x , ::x, x::, ::, etc
///   - v6 with trailing v4 : x:x:x:x:x:x:d.d.d.d where x is in hex and d is in decimal
pub fn from_string(str: String) -> Result(IpAddress, Nil) {
  let tuple(_, res) = eat_ip(<<str:utf8>>, Unknown)
  case res {
    FoundV4(a, b, c, d) -> Ok(IpV4(a, b, c, d))
    FoundV6(a, b, c, d, e, f, g, h) -> Ok(IpV6(a, b, c, d, e, f, g, h))
    _ -> Error(Nil)
  }
}

/// Print an Ip Address to String. Will print either a standard IpV4 or IpV6 address.
pub fn to_string(addr: IpAddress) -> String {
  case addr {
    IpV4(a, b, c, d) ->
      [int.to_string(a), int.to_string(b), int.to_string(c), int.to_string(d)]
      |> string.join(".")
    IpV6(a, b, c, d, e, f, g, h) -> {
      let ints = [a, b, c, d, e, f, g, h]
      let lon = longest_zero_run(ints)
      case lon {
        Error(Nil) ->
          ints
          |> list.map(int_to_hex)
          |> string.join(":")
        Ok(tuple(zeroes_start, zeroes_len)) -> {
          let zeroes_end = zeroes_start + zeroes_len
          let init = case zeroes_start {
            0 -> <<":":utf8>>
            _ -> <<>>
          }
          ints
          |> list.index_fold(
            init,
            fn(i, next, acc) {
              case i {
                // x if x == zeroes_start && i == 7 -> <<acc:binary, "::":utf8>>
                x if x == zeroes_start -> <<acc:binary, ":":utf8>>
                x if x > zeroes_start && x < zeroes_end -> acc
                _ if i == 7 -> <<acc:binary, int_to_hex(next):utf8>>
                _ -> <<acc:binary, int_to_hex(next):utf8, ":":utf8>>
              }
            },
          )
          |> bit_string.to_string()
          |> result.unwrap("")
        }
      }
    }
  }
}

fn int_to_hex(int: Int) -> String {
  assert <<a:4, b:4, c:4, d:4>> = <<int:16>>
  let tuple(_, keep) =
    [a, b, c, d]
    |> list.map(int_to_hex_help)
    |> list.split_while(fn(x) { x == "0" })

  case keep {
    [] -> "0"
    _ -> string.concat(keep)
  }
}

fn int_to_hex_help(int: Int) -> String {
  case int {
    0 -> "0"
    1 -> "1"
    2 -> "2"
    3 -> "3"
    4 -> "4"
    5 -> "5"
    6 -> "6"
    7 -> "7"
    8 -> "8"
    9 -> "9"
    10 -> "a"
    11 -> "b"
    12 -> "c"
    13 -> "d"
    14 -> "e"
    _ -> "f"
  }
}

//
// Private
//
type ParseMode {
  Unknown
  IsV4(List(BitString))
  IsV6(List(BitString))
  FoundV4(Int, Int, Int, Int)
  FoundV6(Int, Int, Int, Int, Int, Int, Int, Int)
  NoIp
}

type SepKind {
  Colon
  Dot
}

// Parse an IP address from the front of a BitString and return it along with
// the rest of the BitString
//
// ## Example Supported Addresses:
// - Standard v4 : d.d.d.d where d is in decimal
// - Standard v6 : x:x:x:x:x:x:x:x where x is in hex
// - v6 with trailing v4 : x:x:x:x:x:x:d.d.d.d where x is in hex and d is in decimal
// 
fn eat_ip(str: BitString, mode: ParseMode) -> tuple(BitString, ParseMode) {
  let tuple(after_num, num) = eat_num(tuple(str, Error(Nil)))
  let tuple(after_sep, sep) = eat_sep(after_num)
  case num {
    Error(Nil) ->
      case mode {
        IsV4(_) -> tuple(after_sep, NoIp)
        IsV6(nums) ->
          case sep {
            Ok(Colon) -> eat_ip(after_sep, IsV6([<<"::":utf8>>, ..nums]))
            _ ->
              case expand_v6(nums) {
                Ok([a, b, c, d, e, f, g, h]) -> tuple(
                  after_sep,
                  FoundV6(a, b, c, d, e, f, g, h),
                )
                _ -> tuple(after_sep, NoIp)
              }
          }
        Unknown ->
          case sep {
            Ok(Colon) ->
              case eat_sep(after_sep) {
                tuple(after_sep_2, Ok(Colon)) ->
                  eat_ip(after_sep_2, IsV6([<<"::":utf8>>]))
                tuple(after_sep_2, _) -> tuple(after_sep_2, NoIp)
              }
            _ -> tuple(after_sep, NoIp)
          }
      }
    Ok(num) ->
      case sep {
        Ok(Dot) ->
          case mode {
            Unknown -> eat_ip(after_sep, IsV4([num]))
            IsV4(parts) ->
              case list.length(parts) {
                1 | 2 -> eat_ip(after_sep, IsV4([num, ..parts]))
                _ -> tuple(after_sep, NoIp)
              }
            IsV6(parts) ->
              // Possible v6 with trailing v4
              case expand_v6_short(parts) {
                Ok([a, b, c, d, e, f]) ->
                  case eat_ip(after_sep, IsV4([num])) {
                    tuple(after_v4, FoundV4(w, x, y, z)) -> {
                      let tuple(g, h) = v4_to_v6_parts(w, x, y, z)
                      tuple(after_v4, FoundV6(a, b, c, d, e, f, g, h))
                    }
                    tuple(after_v4, _) -> tuple(after_v4, NoIp)
                  }
                _ -> tuple(after_sep, NoIp)
              }
            _ -> tuple(after_sep, NoIp)
          }
        Ok(Colon) ->
          case mode {
            Unknown -> eat_ip(after_sep, IsV6([num]))
            IsV6(parts) ->
              case list.length(parts) {
                1 | 2 | 3 | 4 | 5 | 6 | 7 ->
                  eat_ip(after_sep, IsV6([num, ..parts]))
                _ -> tuple(after_sep, NoIp)
              }
          }
        Error(Nil) ->
          case mode {
            IsV4([a, b, c]) ->
              case list.map([c, b, a, num], base_10) {
                [Ok(a), Ok(b), Ok(c), Ok(d)] -> tuple(
                  after_sep,
                  FoundV4(a, b, c, d),
                )
                _ -> tuple(after_sep, NoIp)
              }
            IsV6(nums) ->
              // V6 number end
              case expand_v6([num, ..nums]) {
                Ok([a, b, c, d, e, f, g, h]) -> tuple(
                  after_sep,
                  FoundV6(a, b, c, d, e, f, g, h),
                )
                _ -> tuple(after_sep, NoIp)
              }
            _ -> tuple(after_sep, NoIp)
          }
      }
  }
}

// this converts the 4 segments of a v4 ip address into 2 segments of a
// v6 address for v6 addresses with a trailing v4 address.
fn v4_to_v6_parts(a: Int, b: Int, c: Int, d: Int) -> tuple(Int, Int) {
  tuple(a * 256 + b, c * 256 + d)
}

// a v6 address needs 8 segments
// a v6 segment can either be a hex number or '::'
// '::' represents a series of more than 1 segments with value 0
// if it is a hex number convert it to an int and  use it as a segment
// if it is a '::', calculate how many segments are needed to make 8 total
// and make that many segments of value 0
// 
// ## Examples:
//    ["::"] -> [0,0,0,0,0,0,0,0]
//    ["1","1","::","1","1"] -> [1,1,0,0,0,0,1,1]
//    ["::", "1"] -> [0,0,0,0,0,0,0,1]
//    
fn expand_v6(nums: List(BitString)) -> Result(List(Int), Nil) {
  case list.length(nums) {
    x if x < 1 -> Error(Nil)
    x if x > 8 -> Error(Nil)
    len -> {
      let to_expand = 9 - len
      do_expand_v6(nums, to_expand, Ok([]))
    }
  }
}

// Same as above but for when a v6 address has an embeded v4 address at the end.
// The v4 address takes up 2 segments of the v6 address and so we need to
// expand 6 segments.
fn expand_v6_short(nums: List(BitString)) -> Result(List(Int), Nil) {
  case list.length(nums) {
    x if x < 1 -> Error(Nil)
    x if x > 6 -> Error(Nil)
    len -> {
      let to_expand = 7 - len
      do_expand_v6(nums, to_expand, Ok([]))
    }
  }
}

fn do_expand_v6(
  nums: List(BitString),
  to_expand: Int,
  acc: Result(List(Int), Nil),
) -> Result(List(Int), Nil) {
  case acc {
    Error(_) -> acc
    Ok(ints) ->
      case nums {
        [] -> Ok(ints)
        [num, ..rest] ->
          case num {
            <<"::":utf8>> ->
              do_expand_v6(
                rest,
                to_expand,
                Ok(list.append(list.repeat(0, to_expand), ints)),
              )
            _ ->
              case base_16(num) {
                Ok(n) -> do_expand_v6(rest, to_expand, Ok([n, ..ints]))
                Error(x) -> Error(x)
              }
          }
      }
  }
}

// parses out a v6 number segmunt
// must be in base 16, must be >= 0 and <= 65535
fn base_16(str: BitString) -> Result(Int, Nil) {
  try int =
    str
    |> fold_bytes(
      Ok(0),
      fn(next, acc) {
        case acc {
          Error(_) -> acc
          Ok(i) ->
            case next {
              // 0 - 9
              48 | 49 | 50 | 51 | 52 | 53 | 54 | 55 | 56 | 57 ->
                Ok(i * 16 + { next - 48 })
              // A - F
              65 | 66 | 67 | 68 | 69 | 70 -> Ok(i * 16 + { next - 55 })
              // a - f
              97 | 98 | 99 | 100 | 101 | 102 -> Ok(i * 16 + { next - 87 })
              _ -> Error(Nil)
            }
        }
      },
    )
  case int {
    x if x > 65535 -> Error(Nil)
    _ -> Ok(int)
  }
}

// Parses out a v4 number segment
// must be in base 10, must be >= 0 and <= 255
fn base_10(str: BitString) -> Result(Int, Nil) {
  try int =
    str
    |> fold_bytes(
      Ok(0),
      fn(next, acc) {
        case acc {
          Error(_) -> acc
          Ok(i) ->
            case next {
              // 0 - 9
              48 | 49 | 50 | 51 | 52 | 53 | 54 | 55 | 56 | 57 ->
                Ok(i * 10 + { next - 48 })
              _ -> Error(Nil)
            }
        }
      },
    )
  case int {
    x if x > 255 -> Error(Nil)
    _ -> Ok(int)
  }
}

// like list.fold but for bytes
fn fold_bytes(str: BitString, acc: a, fun: fn(Int, a) -> a) -> a {
  case str {
    <<>> -> acc
    <<next:8, rest:bit_string>> -> fold_bytes(rest, fun(next, acc), fun)
  }
}

// recognizes a separator from the front of the string and returns it along
// with the remainder of the string
fn eat_sep(str: BitString) -> tuple(BitString, Result(SepKind, Nil)) {
  case str {
    <<".":utf8, rest:binary>> -> tuple(rest, Ok(Dot))
    <<":":utf8, rest:binary>> -> tuple(rest, Ok(Colon))
    _ -> tuple(str, Error(Nil))
  }
}

// collects hex charactes at the front of the string and returns them along
// with the remainder of the string
fn eat_num(
  acc: tuple(BitString, Result(BitString, Nil)),
) -> tuple(BitString, Result(BitString, Nil)) {
  case acc {
    tuple(str, num) ->
      case str {
        <<>> -> acc
        <<next:8, rest:binary>> ->
          case next {
            // 0 - 9
            48 | 49 | 50 | 51 | 52 | 53 | 54 | 55 | 56 | 57 ->
              case num {
                Ok(i) -> eat_num(tuple(rest, Ok(<<i:binary, next:8>>)))
                Error(Nil) -> eat_num(tuple(rest, Ok(<<next:8>>)))
              }
            // A - F
            65 | 66 | 67 | 68 | 69 | 70 ->
              case num {
                Ok(i) -> eat_num(tuple(rest, Ok(<<i:binary, next:8>>)))
                Error(Nil) -> eat_num(tuple(rest, Ok(<<next:8>>)))
              }
            // a - f
            97 | 98 | 99 | 100 | 101 | 102 ->
              case num {
                Ok(i) -> eat_num(tuple(rest, Ok(<<i:binary, next:8>>)))
                Error(Nil) -> eat_num(tuple(rest, Ok(<<next:8>>)))
              }
            _ -> acc
          }
      }
  }
}

//
fn longest_zero_run(nums: List(Int)) -> Result(tuple(Int, Int), Nil) {
  nums
  |> list.index_fold(
    [],
    fn(i, n, acc) {
      case n {
        0 ->
          case acc {
            [] -> [tuple(i, 1)]
            [tuple(start, len), ..xs] ->
              case start + len {
                x if x == i -> [tuple(start, len + 1), ..xs]
                _ -> [tuple(i, 1), ..acc]
              }
          }
        _ -> acc
      }
    },
  )
  |> list.fold(
    Error(Nil),
    fn(next, acc) {
      case next {
        tuple(_, 1) -> acc
        tuple(i, n) ->
          case acc {
            Error(_) -> Ok(next)
            Ok(tuple(_, old_n)) if old_n <= n -> Ok(tuple(i, n))
            _ -> acc
          }
      }
    },
  )
}
