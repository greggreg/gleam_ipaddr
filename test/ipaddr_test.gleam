import ipaddr.{IpV4, IpV6, from_string}
import gleam/should
import gleam/bit_string

pub fn from_string_test() {
  from_string("192.168.0.1")
  |> should.equal(Ok(IpV4(192, 168, 0, 1)))
  from_string("192.300.0.1")
  |> should.equal(Error(Nil))
  from_string("192.300.0.1")
  |> should.equal(Error(Nil))

  from_string("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff")
  |> should.equal(Ok(IpV6(
    65535,
    65535,
    65535,
    65535,
    65535,
    65535,
    65535,
    65535,
  )))
  from_string("0:0:0:0:0:0:0:0")
  |> should.equal(Ok(IpV6(0, 0, 0, 0, 0, 0, 0, 0)))
  from_string("a:b:c:d:e:f:1:2")
  |> should.equal(Ok(IpV6(10, 11, 12, 13, 14, 15, 1, 2)))
  from_string("::")
  |> should.equal(Ok(IpV6(0, 0, 0, 0, 0, 0, 0, 0)))
  from_string("ffff::")
  |> should.equal(Ok(IpV6(65535, 0, 0, 0, 0, 0, 0, 0)))
  from_string("fffff::")
  |> should.equal(Error(Nil))
  from_string("f::f:f:f")
  |> should.equal(Ok(IpV6(15, 0, 0, 0, 0, 15, 15, 15)))
  from_string("::f:f:f")
  |> should.equal(Ok(IpV6(0, 0, 0, 0, 0, 15, 15, 15)))
  from_string("f::f:f::f")
  |> should.equal(Error(Nil))
}

pub fn v6_with_trailing_v4_test() {
  from_string("::1.2.3.4")
  |> should.equal(Ok(IpV6(0, 0, 0, 0, 0, 0, 258, 772)))
  assert Ok(ip) = from_string("2001:db8::192.168.0.1")
  ip
  |> ipaddr.to_string()
  |> should.equal("2001:db8::c0a8:1")
}

pub fn to_string_test() {
  //shrink all on null ip
  assert Ok(ip) = ipaddr.from_string("0:0:0:0:0:0:0:0")
  ip
  |> ipaddr.to_string()
  |> should.equal("::")
  //shrink all but 1 right
  assert Ok(ip) = ipaddr.from_string("f:0:0:0:0:0:0:0")
  ip
  |> ipaddr.to_string()
  |> should.equal("f::")
  //shrink all but 1 left
  assert Ok(ip) = ipaddr.from_string("0:0:0:0:0:0:0:f")
  ip
  |> ipaddr.to_string()
  |> should.equal("::f")
  //shrink all but ends
  assert Ok(ip) = ipaddr.from_string("f:0:0:0:0:0:0:f")
  ip
  |> ipaddr.to_string()
  |> should.equal("f::f")
}

pub fn rfc5952_to_string_test() {
  //no leading zeroes
  assert Ok(ip) = ipaddr.from_string("2001:0db8::0001")
  ip
  |> ipaddr.to_string()
  |> should.equal("2001:db8::1")
  // '::' shortens as much as possible
  assert Ok(ip) = ipaddr.from_string("2001:db8:0:0:0:0:2:1")
  ip
  |> ipaddr.to_string()
  |> should.equal("2001:db8::2:1")
  // '::' must not shorten one field
  assert Ok(ip) = ipaddr.from_string("2001:db8:0:1:1:1:1:1")
  ip
  |> ipaddr.to_string()
  |> should.equal("2001:db8:0:1:1:1:1:1")
  // '::' must shorten the longest run of zeroes
  assert Ok(ip) = ipaddr.from_string("2001:0:0:1:0:0:0:1")
  ip
  |> ipaddr.to_string()
  |> should.equal("2001:0:0:1::1")
  // '::' must shorten the left most if longest run of zeroes is a tie
  assert Ok(ip) = ipaddr.from_string("2001:db8:0:0:1:0:0:1")
  ip
  |> ipaddr.to_string()
  |> should.equal("2001:db8::1:0:0:1")
  // a, b, c, d, e, f must be lowercase
  assert Ok(ip) = ipaddr.from_string("::A:B:C:D:E:F")
  ip
  |> ipaddr.to_string()
  |> should.equal("::a:b:c:d:e:f")
}
//
//
// Private Function tests while actively developing
//
// Uncomment this and make ipaddr/eat_num public to test while changing
// pub fn eat_num_test() {
//   ipaddr.eat_num(tuple(bit_string.from_string("123"), Error(Nil)))
//   |> should.equal(tuple(<<>>, Ok(<<"123":utf8>>)))
//   ipaddr.eat_num(tuple(bit_string.from_string("abcdef"), Error(Nil)))
//   |> should.equal(tuple(<<>>, Ok(<<"abcdef":utf8>>)))
//   ipaddr.eat_num(tuple(bit_string.from_string("ABCDEF"), Error(Nil)))
//   |> should.equal(tuple(<<>>, Ok(<<"ABCDEF":utf8>>)))
//   ipaddr.eat_num(tuple(bit_string.from_string("123.123"), Error(Nil)))
//   |> should.equal(tuple(<<".123":utf8>>, Ok(<<"123":utf8>>)))
// }
//
// Uncomment this and make ipaddr/eat_sep public to test while changing
// pub fn eat_sep_test() {
//   ipaddr.eat_sep(<<".":utf8>>)
//   |> should.equal(tuple(<<>>, Ok(ipaddr.Dot)))
//   ipaddr.eat_sep(<<":":utf8>>)
//   |> should.equal(tuple(<<>>, Ok(ipaddr.Colon)))
//   ipaddr.eat_sep(<<",":utf8>>)
//   |> should.equal(tuple(<<",":utf8>>, Error(Nil)))
//   ipaddr.eat_sep(<<":8":utf8>>)
//   |> should.equal(tuple(<<"8":utf8>>, Ok(ipaddr.Colon)))
// }
// 
// Uncomment this and make ipaddr/eat_ip public to test while changing
// pub fn eat_ip_test() {
//   ipaddr.eat_ip(<<"192.168.0.1":utf8>>, ipaddr.Unknown)
//   |> should.equal(tuple(<<>>, ipaddr.FoundV4(192, 168, 0, 1)))
//   ipaddr.eat_ip(<<"192.300.0.1":utf8>>, ipaddr.Unknown)
//   |> should.equal(tuple(<<>>, ipaddr.NoIp))
//   ipaddr.eat_ip(<<"192.300.0.1":utf8>>, ipaddr.Unknown)
//   |> should.equal(tuple(<<>>, ipaddr.NoIp))
//   ipaddr.eat_ip(
//     <<"ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff":utf8>>,
//     ipaddr.Unknown,
//   )
//   |> should.equal(tuple(
//     <<>>,
//     ipaddr.FoundV6(65535, 65535, 65535, 65535, 65535, 65535, 65535, 65535),
//   ))
//   ipaddr.eat_ip(<<"0:0:0:0:0:0:0:0":utf8>>, ipaddr.Unknown)
//   |> should.equal(tuple(<<>>, ipaddr.FoundV6(0, 0, 0, 0, 0, 0, 0, 0)))
//   ipaddr.eat_ip(<<"a:b:c:d:e:f:1:2":utf8>>, ipaddr.Unknown)
//   |> should.equal(tuple(<<>>, ipaddr.FoundV6(10, 11, 12, 13, 14, 15, 1, 2)))
//   ipaddr.eat_ip(<<"::":utf8>>, ipaddr.Unknown)
//   |> should.equal(tuple(<<>>, ipaddr.FoundV6(0, 0, 0, 0, 0, 0, 0, 0)))
//   ipaddr.eat_ip(<<"ffff::":utf8>>, ipaddr.Unknown)
//   |> should.equal(tuple(<<>>, ipaddr.FoundV6(65535, 0, 0, 0, 0, 0, 0, 0)))
//   ipaddr.eat_ip(<<"fffff::":utf8>>, ipaddr.Unknown)
//   |> should.equal(tuple(<<>>, ipaddr.NoIp))
//   ipaddr.eat_ip(<<"f::f:f:f":utf8>>, ipaddr.Unknown)
//   |> should.equal(tuple(<<>>, ipaddr.FoundV6(15, 0, 0, 0, 0, 15, 15, 15)))
//   ipaddr.eat_ip(<<"::f:f:f":utf8>>, ipaddr.Unknown)
//   |> should.equal(tuple(<<>>, ipaddr.FoundV6(0, 0, 0, 0, 0, 15, 15, 15)))
//   ipaddr.eat_ip(<<"f::f:f::f":utf8>>, ipaddr.Unknown)
//   |> should.equal(tuple(<<>>, ipaddr.NoIp))
// }
// pub fn v6_with_trailing_v4_test() {
//   ipaddr.eat_ip(<<"::1.2.3.4":utf8>>, ipaddr.Unknown)
//   |> should.equal(tuple(<<>>, ipaddr.FoundV6(0, 0, 0, 0, 0, 0, 258, 772)))
// }
